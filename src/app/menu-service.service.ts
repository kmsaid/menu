import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MenuServiceService {
  constructor(private http: HttpClient) { }

    private MyMenu = [
      {
      name: 'Salad', choices: [
      { name: 'Santa Fe' },
      { name: 'Greek' },
      { name: 'Asian' },
      ],
      related: [
      {
      name: 'Dressing', choices: [
      { name: 'Italian' },
      { name: 'Blue Cheese' },
      { name: 'Ranch' },
      ]
      },
      {
      name: 'Bread', choices: [
      { name: 'Italian' },
      { name: 'Flat' },
      { name: 'Sourdough' },
      ]
      }
      ]
      },
      {
      name: 'Entree', choices: [
      { name: 'Steak' },
      { name: 'Salmon' },
      { name: 'Rice' },
      ],
      related: [
      ]
      },
      {
      name: 'Soup', choices: [
      { name: 'Minestrone' },
      { name: 'Hot and sour' },
      { name: 'Miso' },
      ],
      related: [
      {
      name: 'Bread', choices: [
      { name: 'Breadsticks'}
      ]
      }
      ]
      }
      ];
  getMenu() {
    return this.MyMenu;
  }
}
