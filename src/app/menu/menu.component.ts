import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuServiceService } from '../menu-service.service';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

public treeData: object[] = [
  {
    name: 'Salad',
    choices: [{
        name: 'Santa Fe'
      },
      {
        name: 'Greek'
      },
      {
        name: 'Asian'
      },
    ],
    related: [{
        name: 'Dressing',
        choices: [{
            name: 'Italian'
          },
          {
            name: 'Blue Cheese'
          },
          {
            name: 'Ranch'
          },
        ]
      },
      {
        name: 'Bread',
        choices: [{
            name: 'Italian'
          },
          {
            name: 'Flat'
          },
          {
            name: 'Sourdough'
          },
        ]
      }
    ]
  }, {
    name: 'Entree',
    choices: [{
        name: 'Steak'
      },
      {
        name: 'Salmon'
      },
      {
        name: 'Rice'
      },
    ],
    related: []
  }, {
    name: 'Soup',
    choices: [{
        name: 'Minestrone'
      },
      {
        name: 'Hot and sour'
      },
      {
        name: 'Miso'
      },
    ],
    related: [{
      name: 'Bread',
      choices: [{
        name: 'Breadsticks'
      }]
    }]
  }];

public treeFields: object = {
  dataSource: this.treeData,
  id: 'name',
  text: 'name',
  child: 'choices',
  showCheckBox: true,
  autoCheck:  true,
};

  constructor(private data: MenuServiceService) { }

  ngOnInit() {
  }

}
